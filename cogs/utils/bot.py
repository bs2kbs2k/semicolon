import asyncio
import datetime
import json
import logging
import os
import pickle
import re
import traceback
import typing
from logging.handlers import QueueHandler, TimedRotatingFileHandler
from queue import SimpleQueue
from sqlite3 import DatabaseError

import aiohttp
import discord
import numpy as np
from discord.ext import commands, tasks
from yaml import load

from cogs.utils.constants import CONSTANTS
from cogs.utils.converters import SnowflakeConvert, Nullable
from cogs.utils.errors import GuildDisabledCommand, SQLError, VariableError, InvalidVariable, UnknownInterpreter, \
    ConverterInitError, VariableParseError, VariableMissing, WebException, ExpectedError
from cogs.utils.help import SemiHelpCommand
from cogs.utils.sql import SQLManager, TableManager
from cogs.utils.tasks import ScheduledTask
from cogs.utils.utils import Localizer, embed_author_template, error_gen, data_path, line_split, init_defaults
from cogs.utils.variables import ContextLike, FakeContext

try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader


class Semicolon(commands.Bot):
    def __init__(self, **kwargs):
        self.config_options = {}

        conf_file = load(open('config.yaml', 'r'), Loader)
        ex_conf_file = load(open('example_config.yaml', 'r'), Loader)
        self.bot_config = init_defaults(conf_file, ex_conf_file)

        os.makedirs(CONSTANTS["data_folder"], exist_ok=True)
        os.makedirs(CONSTANTS["temp_folder"], exist_ok=True)

        self.sql = SQLManager('config')
        self.task_db = TableManager(self.sql, 'tasks', [
            'id string PRIMARY KEY',  # uuid.uuid1().hex
            'run_at integer NOT NULL',  # round([utc datetime object].timestamp())
            'func blob NOT NULL',  # pickled function
            'args blob',  # pickled tuple
            'kwargs blob'  # pickled dict
        ])
        self.gconfig_db = TableManager(self.sql, 'guild_config', [
            'guild integer NOT NULL',  # guild id
            'variable string NOT NULL',  # variable name
            'data blob'  # pickled data
        ])
        self.tasks = {}
        self.i18n = Localizer()

        self.session = aiohttp.ClientSession()

        fmt_str = '[%(asctime)s %(name)s %(levelname)s] %(message)s'
        fmt = logging.Formatter(fmt_str)
        self.logger = logging.getLogger('bot')
        self.logger.setLevel(logging.DEBUG)

        sh = logging.StreamHandler()
        sh.setLevel(logging.INFO)
        sh.setFormatter(fmt)
        self.logger.addHandler(sh)

        self.log_queue = SimpleQueue()
        qh = QueueHandler(self.log_queue)
        qh.setLevel(logging.WARNING)
        qh.setFormatter(logging.Formatter("[%(name)s %(levelname)s] %(message)s"))
        self.logger.addHandler(qh)

        os.makedirs(CONSTANTS['logs_folder'], exist_ok=True)
        fh = TimedRotatingFileHandler(os.path.join(CONSTANTS['logs_folder'], "debug.log"), when='d', backupCount=7)
        fh.setLevel(logging.DEBUG)
        fh.setFormatter(fmt)
        self.logger.addHandler(fh)

        intents = discord.Intents.default()
        intents.typing = False  # spammy & unneeded
        intents.integrations = False  # unneeded
        intents.webhooks = False  # unneeded
        intents.invites = False  # unneeded
        intents.voice_states = False  # unneeded

        # presences rationale: used in test.py but can be sacrificed
        intents.presences = True

        # members rationale: needed for pronouns (join/leave, role cache), joinlogs (join/leave, name change), etc
        intents.members = True

        super().__init__(
            command_prefix=discord.ext.commands.when_mentioned_or(self.bot_config['prefix']),
            description=CONSTANTS['description'],
            case_insensitive=True,
            activity=discord.Game(CONSTANTS['game'].format(self.bot_config['prefix'])),
            allowed_mentions=discord.AllowedMentions(everyone=False, users=False, roles=False),
            intents=intents,
            member_cache_flags=discord.MemberCacheFlags.from_intents(intents),
            help_command=SemiHelpCommand(),
            **kwargs
        )

        self.add_check(self.blocked_guild)

        self.log_chan: discord.TextChannel = None
        self.load_tasks()
        self._log_handler.start()

        self.invite_url: str = None

    def config_get(self, guild_id: int, variable: str, default=None):
        try:
            return pickle.loads(self.gconfig_db.get_row_by({"guild": guild_id, "variable": variable}, cols=['data'])[0])
        except SQLError:
            return default

    async def get_prefix(self, message):
        prefixes = []
        if message.guild:
            prefixes = self.config_get(message.guild.id, 'prefix')
        if not prefixes:
            return await super().get_prefix(message)
        return prefixes

    def get_data_cols(self, raw_data: dict):
        new = []
        cols = []
        for c, n in raw_data.items():
            new.append(n)
            cols.append(c)
        return tuple(new), cols

    def config_set(self, guild_id: int, variable: str, data):
        by = {"guild": guild_id, "variable": variable}
        write = {"data": pickle.dumps(data)}
        try:
            new, cols = self.get_data_cols(write)
            self.gconfig_db.edit_row_by(by, new, cols)
        except (SQLError, DatabaseError):
            # entry doesn't exist, create a new one
            by['data'] = write['data']  # lazy copy over
            new, cols = self.get_data_cols(by)
            self.gconfig_db.create_row(new, cols)

    def config_clear(self, guild_id: int, variable: str) -> bool:
        """Returns true if an entry existed to successfully delete."""
        try:
            self.gconfig_db.delete_row_by({"guild": guild_id, "variable": variable})
            return True
        except SQLError:
            return False

    def validate_variable(self, variable: str) -> str:
        variable = variable.lower().replace('-', '_').replace(' ', '-')
        if variable not in self.config_options:
            raise InvalidVariable(0, variable)
        return variable

    async def interpret_data(self, ctx: ContextLike, variable: str, data: str, ask_confirm: bool = True):
        interpreter = self.config_options[variable]
        try:
            if isinstance(interpreter, commands.Converter):
                params = {}
                if isinstance(interpreter, (SnowflakeConvert, Nullable)):
                    params["ask_confirmation"] = ask_confirm
                p_data = await interpreter.convert(ctx, data, **params)
            elif isinstance(interpreter, type):
                p_data = interpreter.__new__(interpreter, data)
            else:
                raise UnknownInterpreter(interpreter, variable)
        except ConverterInitError as e:
            raise e
        except:
            raise VariableParseError(ctx.guild.id, variable)

        return p_data

    async def get_variable(self, ctx: ContextLike, variable: str, ask_confirm: bool = True):
        variable = self.validate_variable(variable)
        args = (ctx.guild.id, variable)  # these get used a lot
        data = self.config_get(*args)
        if data is None:
            raise VariableMissing(*args)
        return await self.interpret_data(ctx, variable, data, ask_confirm)

    async def set_variable(self, ctx: commands.Context, variable: str, data: str):
        """Sets the value of a variable."""
        variable = self.validate_variable(variable)
        p_data = await self.interpret_data(ctx, variable, data)
        # replaces processed data with a snowflake/ID to save that to the database instead (as discord objects cannot
        #  be pickled)
        o_data = p_data
        if hasattr(o_data, 'id'):
            o_data = getattr(o_data, 'id')
        self.config_set(ctx.guild.id, variable, o_data)
        return p_data

    async def get_join_channel(self, guild: discord.Guild, key: str) -> typing.Union[discord.TextChannel, None]:
        fake_ctx = FakeContext(guild=guild)
        try:
            chan: discord.TextChannel = await self.get_variable(fake_ctx, key+"_channel", ask_confirm=False)
            if not chan:
                chan = None
        except VariableError:
            chan = discord.utils.get(guild.text_channels, name=self.bot_config['channels'][key])
        return chan

    async def on_ready(self):
        self.log_chan = self.get_channel(785356305354588190)

        # overly complicated fancy formatting
        def get_str_length(number):
            number = len(str(number))
            return np.math.ceil(number / 3) - 1 + number

        big_numbers = tuple(map(len, [self.guilds, list(self.get_all_channels()), self.users]))
        guilds, channels, users = big_numbers
        pad_to = max(map(get_str_length, big_numbers))
        pad_str = "{:>" + str(pad_to) + ",}"
        out = [
            self.i18n.localize("login_user").format(self.user),
            self.i18n.localize("login_guilds").format(pad_str.format(guilds)),
            self.i18n.localize("login_channels").format(pad_str.format(channels)),
            self.i18n.localize("login_users").format(pad_str.format(users))
        ]
        self.logger.info("\n".join(out))

        self.invite_url = discord.utils.oauth_url(str(self.user.id), discord.Permissions(
            manage_roles=True,
            view_audit_log=True,
            view_channel=True,
            send_messages=True,
            embed_links=True,
            manage_channels=True,
            manage_emojis=True,
            manage_messages=True,
            add_reactions=True,
            use_external_emojis=True,
            read_message_history=True
        ))

    async def on_command_error(self, ctx: commands.Context, error: Exception):
        delete = 10

        async def generic_error():
            try:
                if ctx.guild and not ctx.me.guild_permissions.embed_links:
                    await ctx.send(str(error))
                else:
                    await ctx.send(embed=error_gen(ctx, str(error)))
            except:
                pass

        if isinstance(error, commands.CommandNotFound):
            pass
        elif isinstance(error, commands.CommandOnCooldown) and ctx.command.name == "catch":
            embed = embed_author_template(ctx, subtle_author=True)
            embed.description = self.i18n.localize('pokemon_ratelimit', ctx=ctx)
            embed.colour = discord.Colour.greyple()
            try:
                await ctx.send(embed=embed)
            except:
                pass
        elif isinstance(error, commands.CommandOnCooldown):
            stopwait = ctx.message.created_at + datetime.timedelta(seconds=error.retry_after)
            chn = ctx.channel
            botself = ctx.guild.me if isinstance(chn, discord.TextChannel) else chn.me
            perms = chn.permissions_for(botself)
            if perms.add_reactions and perms.read_message_history:
                react_index = CONSTANTS['emojis']['RATELIMIT'][int(not perms.use_external_emojis)]
                await ctx.message.add_reaction(react_index)
                thereact = None
                while not thereact:
                    thereact = list(filter(lambda x: str(x) == react_index, ctx.message.reactions))
                    if not thereact:  # avoid sleeping if unnecessary
                        await asyncio.sleep(1)
                await discord.utils.sleep_until(stopwait)
                try:
                    await thereact[0].remove(botself)
                except discord.errors.NotFound:
                    pass
        elif isinstance(error, GuildDisabledCommand):
            e = self.i18n.localize('error_guild_disabled', ctx=ctx).format(ctx.guild.name)
            await ctx.send(e, delete_after=delete)
        elif isinstance(error, commands.NotOwner):
            # await ctx.send("You cannot access that command.")
            pass
        elif isinstance(error, commands.CommandInvokeError):
            if isinstance(error.original, ExpectedError):
                await generic_error()
            else:
                cmention = f"in {ctx.channel.mention}" if isinstance(ctx.channel, discord.TextChannel) else 'in DMs'
                ename = "{0!s} ({1!s})".format(type(error).__name__, type(error.original).__name__)
                tb = ''.join(traceback.format_tb(error.original.__traceback__))
                args = (ename, cmention, ctx.author.mention, ctx.message.content, tb, str(error.original))
                emsg = self.i18n.localize('error_dm').format(*args)

                self.logger.critical(emsg)
                owner: discord.User = (await self.application_info()).owner
                await owner.send(self.i18n.localize("check_logs", user=owner))
                await ctx.send(self.i18n.localize("sorry_error", ctx=ctx).format(ctx.author, ctx.message.content))
        else:
            await generic_error()

    async def blocked_guild(self, ctx):
        """
        Checks if the guild has blocked this command
        """
        if not ctx.guild:
            return True
        if ctx.author.guild_permissions.administrator or ctx.author == ctx.guild.owner:
            return True

        # TODO: should find a way to cache this, i don't imagine this is super efficient?
        filepath = data_path('disabled_commands.json')
        if os.path.exists(filepath):
            with open(filepath) as j:
                data = json.load(j)
            json_key = str(ctx.guild.id)
            if json_key in data:  # if guild is in data
                cmd = ctx.command
                while cmd.parent is not None:  # get the base command
                    cmd = cmd.parent

                if cmd.name in data[json_key]:  # if cmd name is in disabled list
                    raise GuildDisabledCommand
        return True

    async def process_commands_sudo(self, message):
        # From https://gitlab.com/HTSTEM/DiscordBot/-/blob/python-ext.com/cogs/util/bot.py, MIT license
        ctx = await self.get_context(message)
        await self.invoke_sudo(ctx)

    async def invoke_sudo(self, ctx):
        # From https://gitlab.com/HTSTEM/DiscordBot/-/blob/python-ext.com/cogs/util/bot.py, MIT license
        if ctx.command is not None:
            self.dispatch('command', ctx)
            try:
                if 'no_sudo' not in (i.__qualname__.split('.')[0] for i in ctx.command.checks):
                    if isinstance(ctx.command, commands.Group):
                        subcommand = ctx.message.content.replace(ctx.prefix, '', 1)
                        subcommand = ' '.join(subcommand.split(' ')[1:])
                        command = ctx.command.get_command(subcommand)
                        ctx.command = command

                    async def nc(_):
                        return True

                    checks = ctx.command.can_run
                    ctx.command.can_run = nc
                    try:
                        await ctx.command.invoke(ctx)
                    except Exception as e:
                        await ctx.command.dispatch_error(ctx, e)
                    finally:
                        ctx.command.can_run = checks

            except commands.CommandError as e:
                await ctx.command.dispatch_error(ctx, e)
            else:
                self.dispatch('command_completion', ctx)
        elif ctx.invoked_with:
            exc = commands.CommandNotFound(self.i18n.localize("command_not_found", ctx=ctx).format(ctx.invoked_with))
            self.dispatch('command_error', ctx, exc)

    def load_tasks(self):
        for row in self.task_db.get_rows():
            task = ScheduledTask.from_db(self, *row)
            self.tasks[task.id] = task
        self.task_processor.start()

    def add_task(self, run_at, func, *args, **kwargs) -> ScheduledTask:
        task = ScheduledTask(self, run_at, func, *args, **kwargs)
        task.register(self.task_db)
        self.tasks[task.id] = task
        return task

    @tasks.loop(seconds=1.0)
    async def task_processor(self):
        for task in self.tasks.copy().values():
            task: ScheduledTask
            if task.is_ready():
                del self.tasks[task.id]
                self.task_db.delete_row_by({'id': task.id.hex})
                try:
                    await task.execute()
                except:
                    exc = traceback.format_exc()
                    self.logger.error(self.i18n.localize('task_exception') % (task, exc))

    @task_processor.before_loop
    async def before_tasks(self):
        await self.wait_until_ready()

    @tasks.loop(seconds=1.0)
    async def _log_handler(self):
        while not self.log_queue.empty():
            record: logging.LogRecord = self.log_queue.get(False)
            if self.log_chan:
                for m in line_split(record.getMessage()):
                    await self.log_chan.send(m)

    @task_processor.before_loop
    async def _before_logs(self):
        await self.wait_until_ready()
        await asyncio.sleep(1)  # waits for self.logchan to be set

    async def get_api_json(self, url: str, ctx: commands.Context = None) -> dict:
        """
        Returns JSON data from an API.
        :param url: the URL to fetch
        :param ctx: (used for error translation)
        """
        if not re.match(r"https?://", url):
            url = "https://"+url
        async with self.session.get(url, headers={"User-Agent": "semicolon-bot"}) as r:
            if r.status != 200:
                raise WebException(self.i18n, ctx, 'url', r.status, await r.text())
            return await r.json()

    def add_command(self, command: commands.Command):
        # copy from dpy to ensure functionality
        if not isinstance(command, commands.Command):
            raise TypeError('The command passed must be a subclass of Command')
        # add i18n aliases
        lkey = ">" + command.qualified_name
        for ldict in self.i18n.langs.values():
            if lkey in ldict:
                vals = ldict[lkey]
                if not isinstance(vals, list):
                    vals = [vals]
                for val in vals:
                    if val != command.name and val not in command.aliases:
                        command.aliases.append(val)
        # register command
        super().add_command(command)

    async def logout(self):
        self.logger.info("Logging out...")
        self.task_processor.cancel()
        await super(Semicolon, self).logout()

    def run(self):
        token = self.bot_config['token']
        del self.bot_config['token']

        cog_path = CONSTANTS['cog_folder']
        for cog in ('.'.join([cog_path, f.split('.')[0]]) for f in os.listdir(cog_path) if f.endswith('.py') and not f.startswith('_') and f.split('.')[0] not in self.bot_config['cog_blacklist']):
            try:
                self.load_extension(cog)
            except Exception:
                self.logger.exception(self.i18n.localize("cog_load_fail").format(cog))
            else:
                self.logger.info(self.i18n.localize("cog_load").format(cog))

        super().run(token)
