from datetime import datetime

import discord
import humanize
import parsedatetime as pdt

from cogs.utils.bot import Semicolon

units = pdt.pdtLocales['en_US'].units
units['minutes'].append('mins')
units['seconds'].append('secs')
p = pdt.Calendar()


def get_time(time: str, allow_past=False) -> datetime:
    now = datetime.utcnow().replace(microsecond=0)
    time_struct, parse_status = p.parse(time, now)
    if not parse_status:
        raise ValueError("Invalid time input")
    dt = datetime(*time_struct[:6])
    if not allow_past and dt < now:
        raise ValueError("Time is in the past")
    return dt


async def remind(bot: Semicolon, author_id: int, message_id: int, message: str):
    user: discord.User = bot.get_user(author_id)
    if user is None:
        return
    delta = humanize.naturaltime(discord.Object(message_id).created_at, when=datetime.utcnow())
    msg = bot.i18n.localize('reminder', user=author_id).format(delta, message)
    try:
        await user.send(msg)
    except discord.Forbidden:
        pass


async def unmute(bot: Semicolon, user_id: int, guild_id: int, role_id: int):
    guild: discord.Guild = bot.get_guild(guild_id)
    if guild is None:
        return

    me: discord.Member = guild.me
    if not me.guild_permissions.manage_roles:
        return

    role: discord.Role = guild.get_role(role_id)
    if role is None:
        return
    if role >= me.top_role:
        return

    member: discord.Member = guild.get_member(user_id)
    if member is None:
        member = await guild.fetch_member(user_id)
    if member is None:
        return

    if role in member.roles:
        await member.remove_roles(role, reason=bot.i18n.localize("unmute_reason", guild=guild_id))
