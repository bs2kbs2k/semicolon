import typing

import discord
from discord.ext import commands

from cogs.utils.bot import Semicolon
from cogs.utils.utils import get_roles_by_prefix, is_stys, get_input, toggle_role


class Roles(commands.Cog):
    def __init__(self, bot: Semicolon):
        self.bot = bot

    @commands.group(invoke_without_command=True)
    @commands.bot_has_guild_permissions(manage_roles=True)
    @commands.bot_has_permissions(embed_links=True)
    @commands.check(is_stys)
    async def stream(self, ctx):
        """
        Toggles on/off a stream notification role.
        """
        prefix = 'stream-'

        items: typing.List[discord.Role] = get_roles_by_prefix(prefix, ctx.guild)
        if not items:
            await ctx.send(self.bot.i18n.localize('no_stream', ctx=ctx).format(prefix))
            return
        items = list(sorted(items, key=lambda x: x.name.lower()))
        itemtext = [x.mention for x in items]

        role = await get_input(title=self.bot.i18n.localize('stys_stream_title', ctx=ctx),
                               prompt=self.bot.i18n.localize('stys_stream_desc', ctx=ctx),
                               items=itemtext, selections=items, ctx=ctx)
        if role is None:
            return
        await toggle_role(ctx.author, role)

    @commands.command()
    @commands.bot_has_guild_permissions(manage_roles=True)
    @commands.bot_has_permissions(embed_links=True)
    @commands.guild_only()
    async def color(self, ctx: commands.Context):
        """
        Selects a role color if supported by the server.
        Searches for roles prefixed with "color-"
        Optionally, for mods (defined as having "Manage Messages" permissions), searches for roles prefixed with "scolor-"
        """
        default_prefix = 'color-'
        staff_prefix = 's'+default_prefix

        au_perms = ctx.author.guild_permissions
        is_mod = au_perms.administrator or au_perms.manage_messages or ctx.author == ctx.guild.owner
        top_role = ctx.guild.me.top_role

        prefix = staff_prefix if is_mod else default_prefix

        items: typing.List[discord.Role] = get_roles_by_prefix(prefix, ctx.guild)
        if not items and prefix != default_prefix:
            prefix = default_prefix
            items = get_roles_by_prefix(prefix, ctx.guild)
        if not items:
            await ctx.send(self.bot.i18n.localize('no_color', ctx=ctx).format(default_prefix))
            return
        itemtext = [x.mention for x in items]

        existing_role = discord.utils.find(lambda x: x.name.startswith(prefix) and top_role > x, ctx.author.roles)
        if existing_role is not None:
            itemtext.insert(0, self.bot.i18n.localize('color_remove', ctx=ctx))
            items.insert(0, False)

        role = await get_input(title=self.bot.i18n.localize('color_title', ctx=ctx),
                               prompt='', items=itemtext, selections=items, ctx=ctx)
        if role is None:
            return

        if existing_role == role:
            return
        if existing_role is not None:
            await ctx.author.remove_roles(existing_role)
        if role:
            await ctx.author.add_roles(role)


def setup(bot):
    bot.add_cog(Roles(bot))
